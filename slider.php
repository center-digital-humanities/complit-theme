<div id="slider">
	<ul id="bxslider">
		<?php $slides = new WP_Query( array( 'post_type' => 'slider', 'posts_per_page' => 5, 'orderby' => 'date', 'order' => 'desc'	)); while ( $slides->have_posts() ) : $slides->the_post(); 
			$thumb_id = get_post_thumbnail_id();
			$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'home-hero', true);
			$thumb_url = $thumb_url_array[0];
		?>
			<li style="background-image: url('<?php echo($thumb_url) ?>');">
				<div class="content">
					<div class="slider-content">
						<a href="<?php the_field('link'); ?>">
							<h2><?php the_title(); ?></h2>
							<p><?php if(get_field('description')) { ?>
								<?php the_field('description'); ?>
							<?php } ?></p>
						</a>
					</div>	
				</div>
			</li>
		<?php endwhile; ?>
	</ul>
</div>