# _SOL #

_SOL is a Wordpress starter theme developed for use with academic websites. It's based on the [Bones](https://github.com/eddiemachado/bones) starter theme. It comes with some common functionality academic websites might find useful, and has some general styling, but it is meant to be used as a starting point for you to customized as needed for new projects.

## Getting Started ##
I recommend taking the following steps when using this theme:

1. [Download](https://bitbucket.org/tuckerl/_sol-theme/downloads) theme from BitBucket.
2. Install theme in Wordpress.
3. Download the required plugins and optionally the recommended plugins that you'd like to use.
4. Download the Custom Fields for the Advanced Custom Fields plugin and import into Wordpress.
5. (Optional) Download and upload the placeholder content.
6. Customize as you see fit!

## Plugins ##

_SOL makes use of the following plugins:

#### Required ####
* [Advanced Custom Fields Pro](https://wordpress.org/plugins/advanced-custom-fields/) - (Pro version not free) Heavily used for creating custom fields and controlling theme functions.
* [The Events Calendar](https://wordpress.org/plugins/the-events-calendar/) - Integrated into the theme for events.

#### Recommended ####
* [Advanced Access Manager](https://wordpress.org/plugins/advanced-access-manager/) - Manager user roles for Wordpress backend.
* [Broken Link Checker](https://wordpress.org/plugins/broken-link-checker/) - Checks site for broken links and missing images.
* [Contact Form 7](https://wordpress.org/plugins/contact-form-7/) - Simple contact form plugin.
* [Gravity Forms](http://www.gravityforms.com/) - (Not free) For creating web forms.
* [Post Types Order](https://wordpress.org/plugins/post-types-order/) - Allow admin to drag and drop to reorder post.
* [Regenerate Thumbnails](https://wordpress.org/plugins/regenerate-thumbnails/) - Allows you to regenerate all thumbnails after changing the thumbnail sizes.
* [Redirection](https://wordpress.org/plugins/redirection/) - Manage 301 redirects and monitor 404 errors
* [Simple 301 Redirects](https://wordpress.org/plugins/simple-301-redirects/) - Simple way to manage 301 redirects.
* [TablePress](https://wordpress.org/plugins/tablepress/) - Nice way to manage tabular data.
* [WordPress SEO](https://wordpress.org/plugins/wordpress-seo/) - Best way to manage SEO for Wordpress.

## jQuery Plugins ##
The following jQuery plugins are included:

* [Accessible Menu](https://github.com/adobe-accessibility/Accessible-Mega-Menu) - Makes main menu controllable by keyboard.
* [Isotope](http://isotope.metafizzy.co) - For filtering and sorting layouts via AJAX.
* [BxSlider](http://bxslider.com) - Responsive content slider.
* [Modernizr](http://modernizr.com) - Allows you to take advantage of HTML5 and CSS3 features by detecting it in the user’s browser.
* [Sticky-kit](https://github.com/leafo/sticky-kit) - A jQuery plugin for creating smart sticky elements.

## SASS ##
This theme is built using [SASS](http://sass-lang.com/) to write CSS. If you don't want to use SASS, simply ignore the scss directory and instead use the css directory. If you do want to take advantage of SASS and all it's advantages, you'll need a way to compile your SASS into CSS files. There are [many tools](http://mashable.com/2013/06/11/sass-compass-tools/), but if you are using a Mac I recommend [Prepros](http://alphapixels.com/prepros/).

## Downloads ##
Aside from the required plugins, you might also want to download these files to help you get started with your website.

* [Pages](https://ucla.box.com/sample-pages) - Some common pages that might be needed on an academic website with placeholder content.
* [Custom Fields](https://ucla.box.com/sol-custom-fields) - Some custom fields required to make the theme work properly.