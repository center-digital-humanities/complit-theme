<?php get_header(); ?>

			<div class="content main" id="main-content">
				<header>
					<h1><?php single_cat_title(); ?></h1>
					<p><?php echo category_description(); ?></p>
					<div class="filter">
					<?php // To make another filter, duplicate the div below ?>
						<div class="options button-group" data-filter-group="field">
							<ul>
								<?php // Filter should correspond with classes given to different people ?>
								<button class="button btn all is-checked" data-filter="">View All</button>
								<button class="button btn faculty" data-filter=".faculty">Faculty</button>
								<button class="button btn visiting" data-filter=".visiting">Visiting</button>
								<button class="button btn emeriti" data-filter=".emeriti">Emeriti</button>
							</ul>
						</div>
					</div>
					<h2 class="filter-title"><span>View All</span><span class="angle">&nbsp</span></h2>
				</header>			
				<div class="people-list">
					<ul <?php post_class('cf'); ?>>
					<?php $core_loop = new WP_Query( array( 'people_cat' => 'faculty', 'post_type' => 'people', 'posts_per_page' => -1, 'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC')); ?>
					
					<?php while ( $core_loop->have_posts() ) : $core_loop->the_post(); ?>
						<li class="person-item <?php if(get_field('person_type')){ the_field('person_type'); } ?>">
							<a href="<?php the_permalink() ?>">
								<?php // if there is a photo, use it
								if(get_field('photo')) {
									$image = get_field('photo');
									if( !empty($image) ): 
										// vars
										$url = $image['url'];
										$title = $image['title'];
										// thumbnail
										$size = 'people-thumb';
										$thumb = $image['sizes'][ $size ];
										$width = $image['sizes'][ $size . '-width' ];
										$height = $image['sizes'][ $size . '-height' ];
								endif; ?>
								<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo
								<?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
								<?php // otherwise use a silhouette 
								} else { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="A photo of <?php the_title(); ?>" class="photo
								<?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
								<?php } ?>
								<dl>
									<dt class="name"><?php the_title(); ?></dt>
									<dd class="position"><?php the_field('position_title'); ?></dd>
								</dl>
							</a>
						</li>
						
					<?php endwhile; ?>
					
					</ul>
				</div>
			</div>

<?php get_footer(); ?>