<?php get_header(); ?>
			<div id="main-content" role="main">

				<?php // If set to show image at random, we use Gallery custom field
					if(get_field('hero_type', 'option') == "single") {
						$rows = get_field('hero_image' ); // get all the rows
						$rand_row = $rows[ array_rand( $rows ) ]; // get a random row
						
						$home_image = $rand_row['image' ]; // get the sub field value 
						$home_description = $rand_row['description' ]; // get the sub field value 
						$home_button_link = $rand_row['link' ]; // get the sub field value 			
						
						$image = wp_get_attachment_image_src( $home_image, 'home-hero' );
					?>
				<?php if( $home_image ): ?>
				<?php if( $home_button_link ): ?>
				<a href="<?php echo $home_button_link; ?>" class="hero-link">
				<?php endif; ?>
					<div id="hero" style="background-image:url('<?php echo $image[0]; ?>');">
						<div class="content">
						<?php if( $home_description ): ?>
							<div class="hero-description">
								<?php echo $home_description; ?>
							</div>
						<?php endif; ?>
						</div>
					</div>
				<?php if( $home_button_link ): ?>
				</a>
				<?php endif; ?>
				<?php endif; ?>
				<?php } ?>
				
				<?php // If set to slider, we use Slider custom post type
					if(get_field('hero_type', 'option') == "slider") { 
					$max_images = get_field('max_slider_images', 'option');
				?>
				<script type="text/javascript">
					jQuery("document").ready(function($) {
						$(document).ready(function(){
						  $('#bxslider').bxSlider({
						  	autoHover: true,
						  	auto: false,
						  });
						});
					});
				</script>
				<div id="slider">
					<ul id="bxslider">
						<?php $slides = new WP_Query( array( 'post_type' => 'slider', 'posts_per_page' => $max_images, 'orderby' => 'date', 'order' => 'desc')); while ( $slides->have_posts() ) : $slides->the_post(); 
							$thumb_id = get_post_thumbnail_id();
							$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'home-hero', true);
							$thumb_url = $thumb_url_array[0];
						?>
						<?php if(get_field('link')) { ?>
						<a href="<?php the_field('link'); ?>">
						<?php } ?>
							<li style="background-image: url('<?php echo($thumb_url) ?>');">
								<div class="content">
									<div class="slider-content">
										<h2><?php the_title(); ?></h2>
										<p><?php if(get_field('description')) { ?>
											<?php the_field('description'); ?>
										<?php } ?></p>
									</div>
								</div>
							</li>
						<?php if(get_field('link')) { ?>
						</a>
						<?php } ?>
						<?php endwhile; ?>
					</ul>
				</div>
				<?php } ?>
				
				<div class="content">
					<div class="col news-col">
						<h3>Latest News</h3>
						<?php query_posts( array ( 'showposts' => 4	) ); ?>
						<ul>
							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
							
							<a href="<?php the_permalink() ?>">
								<li>
									<?php if ( has_post_thumbnail() ) {
										$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'article-thumb' );
										$url = $thumb['0']; ?>
										<img src="<?=$url?>" alt="<?php the_title(); ?>" />
									<?php } ?>
									<div class="news-item">
										<h4><?php the_title(); ?></h4>
										<p>
											<?php
											$content = get_the_content();
											$trimmed_content = wp_trim_words( $content, 27, '...' );
											echo $trimmed_content;
											?>
										</p>
									</div>
								</li>
							</a>							
							<?php endwhile; ?>
						</ul>
						<?php endif; ?>
						<a class="btn" href="/news/">View All</a>
					</div>
					<div class="col quicklinks-col">
						<h3>Quick Links</h3>
						<?php wp_nav_menu(array(
							'container' => '',
							'menu' => __( 'Quick Links', 'bonestheme' ),
							'menu_class' => 'quick-links',
							'theme_location' => 'quick-links',
							'before' => '',
							'after' => '',
							'link_before' => '<h4>',
							'link_after' => '</h4>',
							'depth' => 0,
							'walker' => new Description_Walker
						)); ?>
					</div>
					<div class="col events-col">
						<ul>
							<?php if ( is_active_sidebar( 'news-sidebar' ) ) : ?>
								<?php dynamic_sidebar( 'news-sidebar' ); ?>				
							<?php else : endif; ?>
						</ul>
					</div>
				</div>
			</div>
<?php get_footer(); ?>