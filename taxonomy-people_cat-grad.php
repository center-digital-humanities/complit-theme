<?php get_header(); ?>

			<div class="content main" id="main-content">
				<header>
					<h1><?php single_cat_title(); ?></h1>
					<p><?php echo category_description(); ?></p>
					<div class="filter">
					<?php // To make another filter, duplicate the div below ?>
						<div class="options button-group" data-filter-group="field">
							<h3>Field of Study</h3>
							<ul>
								<?php // Filter should correspond with classes given to different people ?>
								<button class="button btn all is-checked" data-filter="">View All</button>
								<button class="button btn basketball" data-filter=".basketball">Basketball</button>
								<button class="button btn football" data-filter=".football">Football</button>
								<button class="button btn filter-3" data-filter=".filter-3">Filter 3</button>
								<button class="button btn filter-4" data-filter=".filter-4">Filter 4</button>
								<button class="button btn filter-5" data-filter=".filter-5">Filter 5</button>
								<button class="button btn filter-6" data-filter=".filter-6">Filter 6</button>
							</ul>
						</div>
					</div>
					<h2 class="filter-title">All</h2>
				</header>
				<div class="people-list">
					<ul <?php post_class('cf'); ?>>
					<?php $grad_loop = new WP_Query( array( 'people_cat' => 'grad', 'post_type' => 'people', 'posts_per_page' => -1, 'orderby' => 'meta_value', 'meta_key' => 'last_name', 'order' => 'ASC')); ?>
					
					<?php while ( $grad_loop->have_posts() ) : $grad_loop->the_post(); ?>
						<li class="person-item <?php echo implode(' ', get_field('field')); ?>">
							<a href="<?php the_permalink() ?>">
								<?php // if there is a photo, use it
								if(get_field('photo')) {
									$image = get_field('photo');
									if( !empty($image) ): 
										// vars
										$url = $image['url'];
										$title = $image['title'];
										// thumbnail
										$size = 'people-thumb';
										$thumb = $image['sizes'][ $size ];
										$width = $image['sizes'][ $size . '-width' ];
										$height = $image['sizes'][ $size . '-height' ];
								endif; ?>
								<img src="<?php echo $thumb; ?>" alt="A photo of <?php the_title(); ?>" class="photo
								<?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
								<?php // otherwise use a silhouette 
								} else { ?>
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/silhouette.jpg" alt="A photo of <?php the_title(); ?>" class="photo
								<?php if(get_field('corner_style', 'option') == "circle") { ?> circle<?php } if(get_field('corner_style', 'option') == "rounded") { ?> rounded<?php }?>"/>
								<?php } ?>
								<dl>
									<dt class="name"><?php the_title(); ?></dt>
									<dd class="position"><?php the_field('position_title'); ?></dd>
									<?php if(get_field('field_of_interest')) { ?>
									<dd class="interest">
										<?php the_field('field_of_interest'); ?>
									</dd>
									<?php } ?>
								</dl>
							</a>
						</li>

					<?php endwhile; ?>

					</ul>
				</div>
			</div>

<?php get_footer(); ?>